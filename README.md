# dailybuild-walled

Devs need to use databases and more powerful features. These needs to be highly available but still not open to the public. Hence an authentication wall for [dailybuild.org](dailybuild.org).

We'll be using Google oauth. Building a python ``Flask`` website for server-side rendering fun.

Basic code comes from : [https://realpython.com/flask-google-login/](https://realpython.com/flask-google-login/)
and we'll make it evolve to suit our needs.

## dependencies

```
python3 -m venv env

source env/Scripts/activate

pip install -r requirements.txt
```

## app authentication
Credentials should remain on local machine. They will be consumed via Environment variables.

For local development, you will need to create some credentials for your project : [https://console.cloud.google.com](https://console.cloud.google.com)

```
export GOOGLE_CLIENT_ID=my_google_client_id

export GOOGLE_CLIENT_SECRET=mygoogle_client_secret
```
